# Notepad++ syntax highlighting and Auto Completion for .PRO (Config File for PTC Creo and Wildfire)

Notepad++ syntax highlighting and Syntax Highlighting for .PRO files (Config File for PTC Creo and Wildfire).  
All known options Creo 3.0 M020 (and previous release) are supported.

## Installation


### Syntax Highlighting

1. Open Notepad++.
2. Click in the menu on: "Language -> Define your own Language -> Import".
3. Import the file "syntaxhighlighting/PRO.xml".
4. Restart Notepad++.

### Auto Completion

The steps in the section "Syntax Highlighting" are required for auto completion.

1. Copy the file "autocompletion/PRO.xml" into the folder "C:\Program Files (x86)\Notepad++\plugins\APIs" (or where you installed Notepad++).
2. Restart Notepad++.
3. Open "Settings -> Preferences..." in Notepad++.
4. Go to the tab "Auto-Completion".
5. Choose following options:
  - Enable "Enable auto-completion on each input"
  - Choose "Function completion"
  - Set "From 3th character" (or what you prefer)
